import scrapy, re, traceback
from scrapper.items import MatchStats
from pymongo.connection import MongoClient
from scrapper.settings import DATABASE

class MatchStatsSpider(scrapy.Spider):
    """
        This class implements functions to crawl and scrap instantaneous match data
        from the website. It crawls the website from text files passed as command-line arguments. 
        It then calls parse function that implements CSS
        selector and XPATH to extract relevant information from HTML document. Finally
        data is stored in mongodb database from where it is exported to CSV file after
        cleaning.
    """
    name = "espn_scorecard"
    allowed_domains = ["espncricinfo.com"]
    def __init__(self, filename=None, *args, **kwargs):
        try:
            self.filename = filename
            super(MatchStatsSpider, self).__init__(*args, **kwargs)
            infile = open("scrap_docs/" + self.filename, "r")
            self.start_urls = []
#             self.start_urls = ["http://www.espncricinfo.com/south-africa-v-pakistan-2013/engine/match/567368.html",
#                                "http://www.espncricinfo.com/wcl-championship-2011-13/engine/match/661889.html"]
            for line in infile.readlines():
                self.start_urls.append("http://www.espncricinfo.com" + str(line).strip())    
            infile.close()
            self.dbconnection = MongoClient(DATABASE.get('HOST'), DATABASE.get('PORT'))
            self.db = self.dbconnection[DATABASE.get('NAME')]
        except Exception as e:
            print e
    
    
    def parse(self, response):
        try:
            is_first_batting_home = ""
            full_scorecard = response.css("div#full-scorecard>div.row")
            
            count = 0
            homeTeamItem = MatchStats()
            awayTeamItem = MatchStats()
            homeTeamItem['opposition_allout'] = 0
            awayTeamItem['opposition_allout'] = 0
            
            # There are four section in the page: Match Overview, Innings section, Match Details,
            # match notes 
            for section in full_scorecard:
                count += 1
                if count == 1:
                    # find country for the venue
                    headers = section.css("div.columns div a").extract()
                   
                    country_str = re.sub("<.*?>", "", str(headers[0]))
                    splitarr = country_str.split("in")
                    country, othercountry = None, None
                    if len(splitarr) > 1:
                        country = str((splitarr[1]).split("ODI")[0]).strip()
                        othercountry = str((country_str.split("in")[1]).split("ODI")[0])
                    location = re.sub("<.*?>", "", str(headers[-1]))
                    if country is not None:
                        homeTeamItem['venue_country'] = location + ", " + country
                        awayTeamItem['venue_country'] = location + ", " + country
                    else:
                        homeTeamItem['venue_country'] = location
                        awayTeamItem['venue_country'] = location
                    
                    homeTeamItem['team'] = str(re.sub("<.*?>", "", str(headers[1]))).strip()
                    awayTeamItem['team'] = str(re.sub("<.*?>", "", str(headers[2]))).strip()
                    
                    if homeTeamItem.get('team') == country:
                        homeTeamItem['venue'] = "home"
                        awayTeamItem['venue'] = "away"
                    elif othercountry is not None and awayTeamItem.get('team') == othercountry:
                        homeTeamItem['venue'] = "away"
                        awayTeamItem['venue'] = "home"
                    else:
                        homeTeamItem['venue'] = "neutral"
                        awayTeamItem['venue'] = "neutral"
                    
                    odi = re.sub("<.*?>", "", str(headers[3]))
                    odi = re.findall("[0-9]+", odi)
                    
                    if len(odi) == 1:
                        homeTeamItem['ODI'] = odi[0]
                        awayTeamItem['ODI'] = odi[0]
                    
                elif count == 2:
                      
                    batting_innings = section.css("div.columns table.batting-table")
                    
                    first_batting_country = batting_innings[0].css("th.th-innings-heading").xpath("text()").extract()[0]
                    first_batting_country = str(first_batting_country).split("innings")[0].strip()
                    
                    cond1 = (first_batting_country != awayTeamItem.get('team'))
                    cond2 = (first_batting_country == homeTeamItem.get('team'))
                    is_first_batting_home = False
                    if cond2:
                        is_first_batting_home = True
                    
                    first_batting = section.css("div.columns table.batting-table")[0]
                    first_batting1 = first_batting.re("\([0-9]+\s+wickets.*\).*\n.*</b>")
                    
                    if len(first_batting1) == 0:
                        first_batting1 = first_batting.re(".*\).*\n.*</b>")
                    
                    first_batting = first_batting1
                    if len(first_batting) > 0:
                        run_scored = re.search(".*<b>.*[0-9]+.*</b>.*", first_batting[0])
                        if run_scored is not None:
                            run_scored = str(re.sub("<.*?>", "", run_scored.group())).strip()
                            if cond1:
                                homeTeamItem['runs_conceded'] = int(run_scored)
                                awayTeamItem['runs_scored'] = int(run_scored)
                            else:
                                homeTeamItem['runs_scored'] = int(run_scored)
                                awayTeamItem['runs_conceded'] = int(run_scored)
                        else:
                            raise Exception("First batting data not avail.")
                    
                        wickets = re.search('\([0-9]+\swickets', first_batting[0])
                        
                        if wickets is None:
                            if cond1:
                                homeTeamItem['opposition_allout'] = 1
                                homeTeamItem['wickets_taken'] = 10
                                awayTeamItem['wickets_lost'] = 10
                            else:
                                awayTeamItem['opposition_allout'] = 1
                                awayTeamItem['wickets_taken'] = 10
                                homeTeamItem['wickets_lost'] = 10
                        else:
                            wickets = re.search('[0-9]+', wickets.group())
                            if wickets is not None:
                                if cond1:
                                    homeTeamItem['wickets_taken'] = int(wickets.group())
                                    awayTeamItem['wickets_lost'] = int(wickets.group())
                            else:
                                # scorecard['wickets_taken'] = 0
                                raise Exception("wickets taken not found.\n")
                
                
                    second_batting = section.css("div.columns table.batting-table")[1]
                    
                    second_batting1 = second_batting.re("\([0-9]+\s+wickets.*\).*\n.*</b>")
                    
                    if len(second_batting1) == 0:
                        second_batting1 = second_batting.re(".*\).*\n.*</b>")
                        
                    
                    second_batting = second_batting1
                    if len(second_batting) > 0:
                        run_scored = re.search(".*<b>.*[0-9]+.*</b>.*", second_batting[0])
                        if run_scored is not None:
                            run_scored = str(re.sub("<.*?>", "", run_scored.group())).strip()
                            if cond2:
                                homeTeamItem['runs_scored'] = int(run_scored)
                                awayTeamItem['runs_conceded'] = int(run_scored)
                            else:
                                homeTeamItem['runs_conceded'] = int(run_scored)
                                awayTeamItem['runs_scored'] = int(run_scored)
                        else:
                            raise Exception("Second batting data unavailable")
    
                        wickets = re.search('\([0-9]+\swickets', second_batting[0])
                        if wickets is None:
                            if cond2:
                                homeTeamItem['wickets_lost'] = 10
                                awayTeamItem['wickets_taken'] = 10
                                awayTeamItem['opposition_allout'] = 1
                            else:
                                awayTeamItem['wickets_lost'] = 10
                                homeTeamItem['wickets_taken'] = 10
                                homeTeamItem['opposition_allout'] = 1
                        else:
                            wickets = re.search('[0-9]+', wickets.group())
                            if wickets is not None:
                                if cond2:
                                    homeTeamItem['wickets_lost'] = int(wickets.group())
                                    awayTeamItem['wickets_taken'] = int(wickets.group())
                                else:
                                    awayTeamItem['wickets_lost'] = int(wickets.group())
                                    homeTeamItem['wickets_taken'] = int(wickets.group())
                            else:
                                raise Exception("wickets lost not found.\n")
                        
                    else:
                        
                        raise Exception("Second batting data cannot be read.\nContinuing.....")
                        
                        
                elif count == 3:
                    toss = section.css("div.columns div.match-information div.bold.space-top-bottom-10").extract()[0]
                    toss = re.sub("\n", "", toss)
                    toss = re.sub("<.*?>", "", toss)
                    toss = re.search("Toss\s+-\s+.*,", toss).group()[:-1]
                    toss = toss.split("-")[1].strip()
                    homeTeamItem['toss'] = toss
                    awayTeamItem['toss'] = toss
                    
                elif count == 4:
                   
                    notes = section.css("div.row div.columns ul.notes")
                    homeTeamItem['bowl_pp'] = "1-10"
                    awayTeamItem['bowl_pp'] = "1-10"
                    
                    if len(notes) == 0:
                        if is_first_batting_home == True:
                            homeTeamItem['bat_pp'] = "NA"
                        else:
                            awayTeamItem['bat_pp'] = "NA"
                        # raise Exception("Powerplay details for the match not found.\n")
                    else:
                        powerplay = notes[0].extract()
                        powerplay2 = re.search("Powerplay 2.*\(", powerplay)
                        if powerplay2 is not None:
                            powerplay2 = powerplay2.group()
                            powerplay2 = powerplay2.split(":")[1].strip()[:-1].replace("Overs", "").replace(" ", "")
                            if is_first_batting_home == True:
                                homeTeamItem['bat_pp'] = str(powerplay2)
                            else:
                                awayTeamItem['bat_pp'] = str(powerplay2)
                        else:
                            if is_first_batting_home == True:
                                homeTeamItem['bat_pp'] = "NA"
                            else:
                                awayTeamItem['bat_pp'] = "NA"
                            # raise Exception("Powerplay 2 details not found.\n")
                        
                        powerplay = notes[1].extract()
                        powerplay2 = re.search("Powerplay 2.*\(", powerplay)
                        if powerplay2 is not None:
                            powerplay2 = powerplay2.group()
                            powerplay2 = powerplay2.split(":")[1].strip()[:-1].replace("Overs", "").replace(" ", "")
                            if is_first_batting_home == True:
                                awayTeamItem['bat_pp'] = str(powerplay2)
                            else:
                                homeTeamItem['bat_pp'] = str(powerplay2)
                        else:
                            if is_first_batting_home == True:
                                awayTeamItem['bat_pp'] = "NA"
                            else:
                                homeTeamItem['bat_pp'] = "NA"
                            # raise Exception("Powerplay 2 details not found.\n")
            
                 
            self.db['match_stats'].insert(dict(homeTeamItem))
            self.db['match_stats'].insert(dict(awayTeamItem))
            successfile = open('scrap_docs/success_' + self.filename, 'a')
            if str(self.filename).find('test') >= 0:
                successfile.write('ODI:{0} {1}'.format(homeTeamItem.get('ODI'), response.url))
            else:
                successfile.write(response.url)
            successfile.write("\n")
            successfile.close()
        except Exception as e:
            errorfile = open('scrap_docs/error_' + self.filename , 'a')
            errorfile.write(response.url)
            errorfile.write("\n")
            errorfile.write("msg:" + traceback.format_exc())
            errorfile.write("\n")
            errorfile.close()
            
            
