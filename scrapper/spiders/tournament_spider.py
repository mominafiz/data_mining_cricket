import scrapy

class TournamentURLSpider(scrapy.Spider):    

    name = "espn_tour"
    allowed_domains = ["espncricinfo.com"]
    start_urls = ["http://www.espncricinfo.com/ci/engine/series/index.html?season=2014%2F15;view=season",
                  "http://www.espncricinfo.com/ci/engine/series/index.html?season=2014;view=season",
                  "http://www.espncricinfo.com/ci/engine/series/index.html?season=2013%2F14;view=season",
                  "http://www.espncricinfo.com/ci/engine/series/index.html?season=2013;view=season",
                  "http://www.espncricinfo.com/ci/engine/series/index.html?season=2012%2F13;view=season",
                  ]
    

    def parse(self, response):
        count = 0
        ofile = open('scrap_docs/TourUrls.txt', 'a')
        try:
            
            # get sections for each seasons type such as odi, test etc
            for sel in response.selector.css("div.match-section-head~section"):
                count += 1
                # second section is for ODI
                if count == 2:
                    match_url = sel.css("section.brief-summary div.series-info div.teams a::attr(href)").extract()
                    for m_url in match_url:
                        ofile.write(str(m_url))
                        ofile.write("\n")
                print ".",
            print "\n"
            ofile.close()
        except:
            ofile.close()

