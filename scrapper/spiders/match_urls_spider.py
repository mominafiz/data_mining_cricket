import scrapy


class MatchUrlSpider(scrapy.Spider):
    """
        This class implements functions to crawl and scrap instantaneous match data
        from the website. It reads urls from text file called MatchUrls.txt
        and crawl each website. It then calls parse function that implements CSS
        selector and XPATH to extract relevant information from HTML document. Finally
        data is stored in mongodb database from where it is exported to CSV file after
        cleaning.
    """
    name = "espn_matchurl"
    allowed_domains = ["espncricinfo.com"]
    infile = open('scrap_docs/TourUrls.txt','r')
    start_urls = []
    for line in infile.readlines():
        start_urls.append("http://www.espncricinfo.com" + str(line).strip())
    infile.close()
    def parse(self, response):
        ofile = open('scrap_docs/MatchUrls.txt', 'a')
        try:
            
            for match_url in response.selector.css("div#ciHomeContentlhs div.div630Pad \
                                                    p.potMatchMenuText span:first-child a::attr(href)").extract():
                ofile.write(str(match_url))
                ofile.write("\n")
                print ".",
            print "\n" 
            
        except:
            print("Error in extracting match urls.")
        finally:
            ofile.close()