import scrapy, re, traceback
from scrapper.items import Commentary
from pymongo.connection import MongoClient
from scrapper.settings import DATABASE


class CommentarySpider(scrapy.Spider):
    """
        This class implements functions to crawl and scrap instantaneous match data
        from the website. It reads urls from text file called success_test_urls.txt
        and crawl each website. It then calls parse function that implements CSS
        selector and XPATH to extract relevant information from HTML document. Finally
        data is stored in mongodb database from where it is exported to CSV file after
        cleaning.
    """
    
    # Name of the scrapper.
    name = "espn_commentary"
    allowed_domains = ["espncricinfo.com"]

    def __init__(self, *args, **kwargs):
        super(CommentarySpider, self).__init__(*args, **kwargs)
        self.dbconnection = MongoClient(DATABASE.get('HOST'), DATABASE.get('PORT'))
        self.db = self.dbconnection[DATABASE.get('NAME')]
        self.odi_list = []
        self.odi_idx = 0
        self.innings = 1
        #self.start_urls = ["http://www.espncricinfo.com/wcl-championship-2011-13/engine/match/523482.html?" + 
        #                    "innings=1;page=1;shdr=none;view=commentary;wrappertype=none"]
        self.start_urls = []
        ofile = None
        try:
            ofile = open('scrap_docs/success_test_urls.txt', 'r')
            for line in ofile.readlines():
                fline = re.search('[0-9]+\s+.*', line)
                if fline is not None:
                    split = fline.group().split()
                    self.odi_list.append(split[0])
                    fline = split[1]
                    self.start_urls.append(fline + "?innings=1;page=1;shdr=none;view=commentary;wrappertype=none")
                    self.start_urls.append(fline + "?innings=2;page=1;shdr=none;view=commentary;wrappertype=none")
        except Exception, e:
            print e
        finally:
            if ofile is not None:
                ofile.close()

    def parse(self, response):
        # select innnings of the game.
        if response.url.find('innings=1') > -1:  
            self.odi_idx += 1
        # get all division with class name 'end-of-over-info'
        end_of_over = response.css("#commInnings > div.commentary-section div.end-of-over-info")
        
        game_snapshot = end_of_over
        batsmen_stats = end_of_over
        game_snapshot = game_snapshot.css("p span:nth-child(2)").extract()
        
        # get all Html tags that have batmsan statistics for each over.
        batsmen_stats = batsmen_stats.css("ul.end-of-over-player-stat ul.end-of-over-batsmen li.clearfix")
        
        try:
            batsmen_stat = []
            for i in range(0, len(batsmen_stats)):
                firstb = batsmen_stats[i].css("span.columns").extract()                
                for x in range(len(firstb)):
                    batsmen_stat.append(firstb[x])

            new_batsmen_stat = []
            # remove empty tags from the list.
            for i in range(len(batsmen_stat)):
                v = re.search("><", batsmen_stat[i])
                if v is None:
                    new_batsmen_stat.append(batsmen_stat[i])
                    
            batsmen_stat = new_batsmen_stat            
            for i in range(0, len(batsmen_stat) - 4, 4):
                if len(batsmen_stat) - i < 4:
                    continue
                # Create data structure object and initialize values (default)
                batsman1, batsman2 = Commentary(), Commentary()
                batsman1['batsman'] = ""
                batsman1["batsman_cluster"] = 0
                batsman1['home_runs'] = 0
                batsman1['non_home_runs'] = 0
                batsman1['runs'] = 0
                batsman1['balls'] = 0
                    
                batsman2['batsman'] = ""
                batsman2["batsman_cluster"] = 0
                batsman2['home_runs'] = 0
                batsman2['non_home_runs'] = 0
                batsman2['runs'] = 0
                batsman2['balls'] = 0
                

                game_idx = (i / 4) + 1
                game_snap = ""
                try:
                    game_snap = re.sub("<.*?>", "", str(game_snapshot[game_idx - 1]))
                    game_snap = re.search("[0-9]+/[0-9]+", str(game_snapshot[game_idx - 1])).group()
                except:
                    pass
                
                batsman1['over'] = game_idx
                batsman2['over'] = game_idx
                batsman1['ODI'] = self.odi_list[self.odi_idx]
                batsman2['ODI'] = self.odi_list[self.odi_idx]
                 
                hr = 0
                batsman1['batsman'] = re.sub("<.*?>", "", str(batsmen_stat[i]))
                stats = re.sub("<.*?>", "", str(batsmen_stat[i + 1]))
                
                if re.search("\s*[0-9]+", stats):
                    batsman1['runs'] = int(re.search("\s*[0-9]+", stats).group().strip())
                    
                if re.search("\([0-9]+b", str(batsmen_stat[i + 1])):
                    batsman1['balls'] = int(re.search("\([0-9]+b", str(batsmen_stat[i + 1])).group()[1:-1])
                
                if re.search("[0-9]+x4", str(batsmen_stat[i + 1])):
                    r = int(re.search("[0-9]+x4", str(batsmen_stat[i + 1])).group()[:-2])
                    batsman1['home_runs'] += r
                    hr += r * 4
                    
                if re.search("[0-9]+x6", str(batsmen_stat[i + 1])):
                    r = int(re.search("[0-9]+x6", str(batsmen_stat[i + 1])).group()[:-2])
                    batsman1['home_runs'] += r
                    hr += r * 6
                nhr = int(batsman1.get('runs')) - hr
                batsman1['game_snapshot'] = game_snap
                batsman1['non_home_runs'] += nhr
                
                hr = 0
                batsman2['batsman'] = re.sub("<.*?>", "", str(batsmen_stat[i + 2]))
                stats = re.sub("<.*?>", "", str(batsmen_stat[i + 3]))
                if re.search("\s*[0-9]+", stats):
                    batsman2['runs'] = int(re.search("\s*[0-9]+", stats).group())
            
                if re.search("\([0-9]+b", str(batsmen_stat[i + 3])):
                    batsman2['balls'] = int(re.search("\([0-9]+b", str(batsmen_stat[i + 3])).group()[1:-1])
                
                if re.search("[0-9]+x4", str(batsmen_stat[i + 3])):
                    r = int(re.search("[0-9]+x4", str(batsmen_stat[i + 3])).group()[:-2])
                    batsman2['home_runs'] += r
                    hr += 4 * r
                if re.search("[0-9]+x6", str(batsmen_stat[i + 3])):
                    r = int(re.search("[0-9]+x6", str(batsmen_stat[i + 3])).group()[:-2])
                    batsman2['home_runs'] += r
                    hr += 6 * r
                
                nhr = int(batsman2.get('runs')) - hr
                batsman2['non_home_runs'] += nhr
                batsman2['game_snapshot'] = game_snap
                
                if "innings=1" in response.url:
                    batsman1['innings'] = 1
                    batsman2['innings'] = 1
                if "innings=2" in response.url:
                    batsman1['innings'] = 2
                    batsman2['innings'] = 2
                

                bo1 = batsman1.get('over')
                bo2 = batsman2.get('over')
                if bo1 <= 50:
                    self.db['inst_match'].insert(dict(batsman1))
                if bo2 <= 50:
                    self.db['inst_match'].insert(dict(batsman2))
            
        except Exception as e:
            print traceback.print_exc()      
