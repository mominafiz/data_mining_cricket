# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
    
class MatchStats(scrapy.Item):
    """
        Data structure that stores historical match data scrapped data from the website.    
    """
    ODI = scrapy.Field()
    team = scrapy.Field()
    runs_scored = scrapy.Field()
    wickets_lost = scrapy.Field()
    runs_conceded = scrapy.Field()
    wickets_taken = scrapy.Field()
    opposition_allout = scrapy.Field()
    # after 2012 two powerplays only, before three
    bat_pp = scrapy.Field()
    bowl_pp = scrapy.Field()
    venue = scrapy.Field()
    toss = scrapy.Field()
    venue_country = scrapy.Field()
    
class Commentary(scrapy.Item):
    """
        Data structure that stores instantaneous match data. i.e batsman and game snapshot
    """
    ODI = scrapy.Field()
    batsman = scrapy.Field()
    batsman_cluster = scrapy.Field()
    runs = scrapy.Field()
    home_runs = scrapy.Field()
    non_home_runs = scrapy.Field()
    balls = scrapy.Field()
    over = scrapy.Field()
    game_snapshot = scrapy.Field()
    innings = scrapy.Field()