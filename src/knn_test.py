
import csv
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import BaggingClassifier
from knn import main

# call to knn implementation
rtuples = main()
with open('data/batsman_stats_csv.csv') as csv_file:
    data_file = csv.reader(csv_file)
    temp = next(data_file)
    target_names = np.array(['BattingAvg', 'StrikeRate', 'HRA', 'MRA'])

    trainset = rtuples[1]
    n_samples = len(trainset)
    n_features = len(trainset[0])-1
    train_data = np.empty((n_samples, n_features))
    train_target = np.empty((n_samples,), dtype=np.float)
    for i in range(0,len(trainset)):
        train_data[i] = np.asarray(trainset[i][:-1], dtype=np.float)
        train_target[i] = np.asarray(trainset[i][-1], dtype=np.int)

    testset = rtuples[2]
    n_samples = len(testset)
    n_features = len(testset[0])-1
    test_data = np.empty((n_samples, n_features))
    test_target = np.empty((n_samples,), dtype=np.float)
    for i in range(0,len(testset)):
        test_data[i] = np.asarray(testset[i][:-1], dtype=np.float)
        test_target[i] = np.asarray(testset[i][-1], dtype=np.int)
        
    # SKLearn (machine learning package) implementation
    clf2 = KNeighborsClassifier(n_neighbors=5,metric="euclidean")
    clf2.fit(train_data,train_target)
    results = clf2.predict(test_data)
    
    matches = 0
    for i in range(n_samples):
        if test_target[i]==results[i]:
            matches +=1
    print "SciPy KNN:    " + str((matches/float(n_samples))*100)

    
    # Optimization over 
    eclf = BaggingClassifier(clf2, max_samples=1.0, max_features=0.7, bootstrap=False)
    eclf.fit(train_data, train_target)
    results = eclf.predict(test_data)
    matches = 0
    for i in range(n_samples):
        if test_target[i]==results[i]:
            matches +=1
    print "Ensemble bagging algorithm:    " + str((matches/float(n_samples))*100)

# output training and test dataset in csv file for use
ofile = open("data/batsman_stat_training.csv","w")
training = rtuples[1]
for row in training:
    r = [str(i) for i in row]
    ofile.write(",".join(r))
    ofile.write("\n")
ofile.close()

ofile = open("data/batsman_stat_test.csv","w")
test = rtuples[2]
for row in test:
    r = [str(i) for i in row]
    ofile.write(",".join(r))
    ofile.write("\n")
ofile.close()