import csv
import random
import math
import operator


# handle the data. read the data file and convert it to csv & display it
# split test & training dataset, convert string values to integer


def loadDataset(filename, split=0, trainingSet=[], testSet=[]):
    with open(filename, 'rb') as csvfile:
        lines = csv.reader(csvfile)
        dataset = list(lines)
        for x in range(len(dataset)-1):
            for y in range(len(dataset[x])-1):
                dataset[x][y] = float(dataset[x][y])
            if random.random() < split:
                trainingSet.append(dataset[x])
            else:
                testSet.append(dataset[x])

# calculate the similarity in order to make predictions. Use euclidean distance measure
def euclideanDistance(inst1, inst2, length):
    distance = 0
    for x in range(length):
        distance += pow((inst1[x] - inst2[x]), 2)
    return math.sqrt(distance)

# test euclideanDistance with some sample data


# find k most similar instances (neighbors)


def getNeighbors(trainingSet, testInstance, k):
    distances = []
    length = len(testInstance) - 2
    
    for x in range(len(trainingSet)):
        dist = euclideanDistance(testInstance, trainingSet[x], length)
        distances.append((trainingSet[x], dist))
    distances.sort(key = operator.itemgetter(1))
    neighbors = []
    for x in range(k):
        neighbors.append(distances[x][0])
    return neighbors

# test the getNeighbors function
# devise predicted response based on neighbors


def getResponse(neighbors):
    classVotes = {}
    for x in range(len(neighbors)):
        response = neighbors[x][-1]
        if response in classVotes:
            classVotes[response] += 1
        else:
            classVotes[response] = 1
        sortedVotes = sorted(classVotes.iteritems(), key = operator.itemgetter(1))
        return sortedVotes[0][0]

# test the response
# classification accuracy, ratio of total correct predictions out of all predictions


def getAccuracy(testSet, predictions):
    correct = 0
    for x in range(len(testSet)):
        if testSet[x][-1] == predictions[x]:
            correct += 1
    return (correct/float(len(testSet))) * 100

# test the accuracy

def main(testdata=[],k=5,split = 0.67):
    # prepare the data
    trainingSet = []
    testSet = []
    if len(testdata)!=0:
        split = 1.0
    loadDataset('data/batsman_stats_normalized.csv', split, trainingSet, testSet)
    
    if len(testdata)!=0:
        testSet = testdata
#     trainingSet.extend(testSet[:(int(len(testSet)*0.20))])
    print "\nTrain Set: " + repr(len(trainingSet))
    print "Test Set: " + repr(len(testSet)) + "\n"
    
    #generate predictions
    predictions = []
    for x in range(len(testSet)):
        neighbors = getNeighbors(trainingSet, testSet[x], k)
        result = getResponse(neighbors)
        predictions.append(result)
    
    accuracy = getAccuracy(testSet, predictions)
    print "Accuracy of our implementation: " + repr(accuracy) + "%"
    
#     if len(testdata)>0:
    return predictions,trainingSet,testSet

#main(k=5,split=0.67)