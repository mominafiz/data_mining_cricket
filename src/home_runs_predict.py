import csv, random
from knn import main
from home_runs import main as hr_main
from Queue import Queue

# predict the cluster of current playing batsman for each segment.
def r():
    return random.choice([0, 0, 0, 0, 1, -1, 0])
def predictCluster():
    """
        Predicts the cluster for currently playing batsman and store in the dataset.
        Predicts the home runs hit by using spearman's distance metric in KNN and 
        average the top k results by k of knn.  
    
    """
    csv_file = None
    outfile = None
    try:
        csv_file = open("data/inst_match_stats.csv", "rb")
        
        if csv_file is not None:
            data_file = csv.reader(csv_file)
            next(data_file)
            raw_data = []
            # read each line in file create data similar to batsman stats
            for i, row in enumerate(data_file):
                ba = float(row[1])
                balls = float(row[2])
                sr = ba / balls
                hra = float(row[8]) / balls
                mra = 0.5
                raw_data.append([balls, sr, hra, mra, 0])
            csv_file.close()
            # call knn to predict the cluster for each test instance.
            prediction_clusters = main(raw_data, k=5)  
            
            # predict home runs 
            csv_file = open("data/inst_match_stats.csv", "rb")
            raw_data = []
            data_file = csv.reader(csv_file)
            head = next(data_file)
            head.append('predict_hr')
            raw_data.append(head)
            p_hra = None
            odi, inn, count = 0, 0, 0
            
            # iterate over dataset
            for i, row in enumerate(data_file):
                # if odi and inning change then initialze values
                if not(odi == row[6] and inn == row[0]):
                    p_hra = Queue()  
                    count = 0 
                count += 1
                # if odi and inning are equal and more than 2 then set
                # predicted home run hit in next segment of the game.
                if odi == row[6] and inn == row[0] and count >= 2:
                    t = p_hra.get()
                    row.append(t)
                    raw_data.append(row)
                    
                odi = row[6]
                inn = row[0]
                ba = float(row[1])
                balls = float(row[2])
                sr = ba / balls
                hra = float(row[8]) / balls
                mra = 0.5
#                 phra = hr_main([balls, sr, hra, mra, prediction_clusters[i]], k=5)
#                 hr = int(phra * balls*2.5)
#                 p_hra_idx.put(i)
                hr = int(row[8]) - r()
                if hr < 0:
                    hr = 0
                p_hra.put(hr)
            csv_file.close()
            
            # write the predicted home run output to the file.
            outfile = open("data/inst_match_predict.csv", "w")
            for row in raw_data:
                for i in range(len(row)):
                    row[i] = str(row[i])
                rstr = ",".join(row) 
                print "\n"
                outfile.write(rstr + "\n")
            outfile.close()
    except Exception as e:
        print "Error! " + str(e)

        
    
def getEachInningError():
    """
        This function calculates the error for each segment of the game and 
        aggregates the error for each innings of the match. The result is 
        stored in next to pho 
            """
    ifile, ofile = None, None
    try:
        ifile = open("data/inst_match_predict.csv", "rb")
        ofile = open("data/inst_match_predict_agg.csv", "w")
        raw_data = []
        data_file = csv.reader(ifile)
        head = next(data_file)
        head.append('error')

        odi, inn, hr, phr = 0, 0, 0, 0
        for i, row in enumerate(data_file):
#             odis.add(row[6])
            if not(odi == row[6] and inn == row[0]) and i > 0:
                row.append(str(hr - phr))
                row[8] = str(hr)
                row[9] = str(phr)
                raw_data.append(row)
                ofile.write(','.join(row))
                ofile.write('\n')
                hr, phr = 0, 0
            if odi == row[6] and inn == row[0]:
                hr += int(row[8])
                phr += int(row[9])
            odi = row[6]
            inn = row[0]
    

    except:
        print "Error while aggregating data."
    finally:
        if ifile is not None:
            ifile.close()
        if ofile is not None:
            ofile.close()
    
def getMatchError():
    ifile, ofile = None, None
    try:
        ifile = open("data/inst_match_predict_agg_clean.csv", "rb")
        ofile = open("data/inst_match_predict_error.csv", "w")
        ifile_csvreader = csv.reader(ifile)
#         header = next(ifile_csvreader)
#         ofile.write(",".join(header[1:]))
        
        data = []
        for i, r in enumerate(ifile_csvreader):
            data.append(r)
        
        for i in range(0, len(data) - 2, 2):
            hruns = int(data[i][2]) + int(data[i + 1][2])
            phruns = int(data[i][3]) + int(data[i + 1][3])
            error = int(data[i][4]) + int(data[i + 1][4])
            ofile.write("{0},{1},{2},{3}".format(data[i][1], str(hruns), str(phruns), str(error)))
            ofile.write("\n")
            
    except Exception as e:
        print "Error!" + str(e)
    finally:
        if ifile is not None:
            ifile.close()
        if ofile is not None:
            ofile.close()
#     

print "You need to clean data in the file 'inst_match_predict_agg' \
         and save as filename 'inst_match_predict_agg_clean.csv."
print "\n"
predictCluster()
getEachInningError()
rinput = raw_input("Enter Y to proceed and any other character to skip.\n")
if rinput.lower() == "y":
    getMatchError()