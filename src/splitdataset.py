import random
def createTestAndTrainingData(splitratio=0.75):
    infile = None
    trainfile = None
    testfile = None
    try:
        infile = open("scrap_docs/MatchUrls.txt","r")
        matchurls = infile.readlines()
        totalurls = len(matchurls)
        
        if totalurls < 100:
            raise Exception("Insufficient data to create test and training dataset.")
        random.shuffle(matchurls)
        
        total_trainurls = int(totalurls*splitratio)
        
        trainfile = open('scrap_docs/training_urls.txt', 'w')
        for i in range(total_trainurls):
            trainfile.write(matchurls[i])
        
        testfile = open("scrap_docs/test_urls.txt", 'w')
        for i in range(total_trainurls,totalurls):
            testfile.write(matchurls[i])
        
    except Exception as e:
        print e
    finally:
        if infile is not None:
            infile.close()
        if trainfile is not None:
            trainfile.close()
        if testfile is not None:
            testfile.close()

createTestAndTrainingData()