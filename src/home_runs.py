import csv
import random
import math
import operator


# handle the data. read the data file and convert it to csv & display it
# split test & training dataset, convert string values to integer


def loadDataset(filename, split=0, trainingSet=[], testSet=[]):
    # Removed Milestone hitting ability from training and test
    with open(filename, 'rb') as csvfile:
        lines = csv.reader(csvfile)
        dataset = list(lines)
        for x in range(len(dataset)-1):
            for y in range(len(dataset[x])-1):
#                 if y !=3:
                dataset[x][y] = float(dataset[x][y])
            if random.random() < split:
                trainingSet.append(dataset[x])
            else:
                testSet.append(dataset[x])

# Display the train and test data

# calculate the similarity in order to make predictions. Use euclidean distance measure


def spearmanDistance(inst1, inst2, length):
    distance = 0
    for x in range(length):
        distance += pow((inst1[x] - inst2[x]), 2)
    return math.sqrt(distance)

# test euclideanDistance with some sample data
# find k most similar instances (neighbors)


def getNeighbors(trainingSet, testInstance, k):
    distances = []
    length = len(testInstance) - 2
    
    for x in range(len(trainingSet)):
        dist = spearmanDistance(testInstance, trainingSet[x], length)
        distances.append((trainingSet[x], dist))
    distances.sort(key = operator.itemgetter(1))
    neighbors = []
    for x in range(k):
        neighbors.append(distances[x][0])
    return neighbors

# test the getNeighbors function

def main(testInst,k=5,split = 1, computed_ds=None):
    
    trainingSet = []
    testSet = []
    if computed_ds is  None:
        loadDataset('data/batsman_stats_csv.csv', split, trainingSet, testSet)
    
#     print "\nTrain Set: " + repr(len(trainingSet))
    testSet.append(testInst)
#     print "Test Set: " + repr(len(testSet)) + "\n"

    #generate predictions
    hr = 0
    for x in range(len(testSet)):
        neighbors = getNeighbors(trainingSet, testSet[x], k)
        for n in neighbors:
            hr += n[2]
        hr = hr/k
    
    return hr
    
