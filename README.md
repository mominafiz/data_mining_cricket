*Copyrights under BSD 3 Clause  |  Contact: Afiz Momin (mominafiz@gmail.com)*

[Refer this articles for more details](http://epubs.siam.org/doi/abs/10.1137/1.9781611973440.121)


## **The Analytical Edge to Cricket: Using KNN algorithm** ##

**
What is this program for?**
This project helps to predict a cluster of the currently playing batsman in the game of cricket using K-Nearest Neighbour algorithm. It then predicts home runs scored in next segment the game by currently playing batsman. This program is written in python and developed using Eclipse IDE.

### Data Extraction: ###

		We have used the web scrapping library called 'scrapy' to implement scrapping and extract relevant information from the website. After extraction, data is stored in MongoDB (database) where using it we have cleaned data.
		
		Project Structure for Scrapping:
		
		scrapper:			It is the main package that holds configuration and webscrapping package.
			__init__.py		This file is the default for python package unless it will be a folder.
			items.py		This the file in which data structure is defined which allows storing
							extracted data and then stored in mongodb database.
			pipelines.py	(Restrained from using this file in this project.)
			settings.py		Configuration for scrapy and mongodb connection information.
			
			spiders:					This the package where modules for web scrapping are written.
				__init__.py				This file is the default for python package unless it will be folder.
				tournament_spider.py	The urls for year archive between 2011-2015 are start url for crawling 
										One Day International (ODI) series/tour and stored in file
										scrap_docs/TourUrls.txt
				match_urls_spider.py	The urls for ODI series/tour crawled by above crawler were input to 
										this crawler. It then extract urls for all matches in series/tour
										for each urls found in TourUrls.txt. Extracted urls are stored in
										scrap_docs/MatchUrls.txt file.
				scorecard_spider.py		The urls in MatchUrls.txt were divided into training_urls.txt and 
										test_urls.txt. The implementation logic is in src/splitdataset.py.
										During extraction there is high possibility of error for url being
										crawled which are stored in scrap_docs/error_training_urls.txt, and
										scrap_docs/error_test_urls.txt. Sucess urls were stored in sucess files. 
										The urls for each matches in both success files 
										are input to this crawler and data from this webpage is scrapped and 
										stored in format to create Historical Facts (match statistics). The 
										data extracted is stored in mongodb database in collection/table called 
										'match_stats'. 
				commentary_spider.py	Instantaneous match statistics for each over in the game is extracted 
										for urls stored in data/success_test_urls.txt and stored in mongodb
										under collection/table called 'inst_match'
										
		src:				It is the main package for algorithms implementation and other 
							supporting implementation.
			clean.py		To re-do scrapping the text files that stores URL needs to be
							truncated for re-use. This modules helps to do that.
			splitdataset.py	This module is written to support scrapper 
			pretty.py		The historical batsman stats between year 2011-2015 downloaded and
							stored in csv file data/batsman_stats_csv.csv is modified to input
							batsman cluster, remove errors and batsman's name.
		
### Data Mining: ###

	Project structure for data mining :
	==================================

		src:						It is the main package for algorithms implementation and other 

									supporting implementation.
			__init__.py				This file is the default for python package unless it will be folder.
			knn.py					The algorithm for k-nearest neighbor (KNN) is implemented in this module.
				
			knn_test.py				This is the main module that runs above module. It also checks the performance
									our implementation with third-party sklearn library. To improve efficiency 
									of our implementation we used Ensemble Bagging (AB) algorithm with KNN as
									base-estimator and prints the comparison of the output.
			home_runs.py			In this module we have modified KNN which use Spearman distance metric
									instead of Euclidean to get top 5 neighbors for testInstance. The avg of
									home runs hitting ability (hra) was used to predict home runs to be hit in 
									next segement of the game. 
			home_runs_predict.py	The home runs hitting ability of the batsman were determined by first
									identifying the cluster of an algorithm

	Algorithm:
	==========
		knn.py
			1.	loadDataset()		loads the dataset and divide into training and test dataset
			2.	euclideanDistance()	calculates the distance for each testInstance with trainInstance
			3.	getNeighbors()		calculates distance by calling function (2) and get k-neighbors
			4.	getResponse()		does hards voting to predict cluster
			5.	getAccuracy()		calculates accuracy.
			6.	main()				program runner.				
	 
	 	Ensemble Attribute Bagging Algorithm:
	 		1. load training dataset and test dataset used in knn algorithm.
	 		2. initialize with KNN, replacement factors for samples and features.
	 		3. Load training set using fit()
	 		4. Load test set using predict()
	 	
	 	data folder:
			batsman_stats_csv.csv			batsman statistical data (downloaded)
			batsman_stats_normalized.csv	batsman statistical data normalized
			match_stats.csv					Historical match statistical information exported from MongoDb:
											cmd: mongoexport --db odi_dmdw --collection match_stats --csv -f ODI,
											team,toss,venue,runs_scored,runs_conceded,wickets_lost,wickets_taken,
											opposition_allout,bowl_pp,bat_pp --out match_stats.csv
			inst_match_stats.csv			Instantaneous match data exported from Mongodb:
											cmd: mongoexport --db odi_dmdw --collection inst_match_intrvl --csv
											-f runs,innings,balls,batsman_cluster,game_snapshot,batsman,
											ODI,overs,home_runs --out filename.csv --out inst_match_stats.csv

### How to run the program? ###
	There are series of step and sometimes it is complicated and requires manual interaction.
	
	NOTE: Please read installation guide before proceeding.
	
	Scrap the data:
	 	Only do this if you want to create new dataset because it requires manual interaction 
	 	to clean the data) Take a backup of the dataset in the data folder.
	 	
	 	Clean files:
	 		python src/clean.py:	Cleans files in scrap_docs
	 	
	 	Navigate to the root of the project. i.e dmdw_project
		 	Cmd: scrapy crawl espn_tour
		 	Cmd: scrapy crawl espn_matchurl
	 		Cmd: python src/splitdataset.py
	 		Cmd: scrapy crawl espn_scorecard -a scrap_docs/training_urls.txt
	 		Cmd: scrapy crawl espn_scorecard -a scrap_docs/test_urls.txt
	 		Cmd: scrapy crawl espn_commentary 
							
		After exporting data as explained in data folder section, run algorithm using the following command:
			Cmd: python knn_test.py
			Cmd: python home_runs.py	You need to clean the data when prompted on a command line.
		
		Output data: 
			inst_match_predict.csv			After running prediction using home_runs_predict.py the output is 
											stored in column next to home_runs with column name 'predicted_home_runs'
			inst_match_predict_agg.csv		After prediction of home runs hit for the next segment, the home runs 
											for an inning is aggregated and stored in this file. Error is calculate and
											this file is use for visualization and other statistical analysis.
			inst_match_predict_clean.csv	Once we have aggregated data for two innings of each game,we clean the data
											manually by checking each game with two innings. Remove the data with missing
											innings.
			inst_match_predict.csv			we now aggregate home runs and predicted home runs for each match. This file
											is used for visualization and other error analysis.
		Reports:
			Reports were created using excel spreadsheet and using R plot.
			
			Create home run report by copying last column from inst_match_predict_agg.csv and inst_matc_predict.csv
			into file results/std_dev_agg.csv and results/std_dev_agg1.csv respectively.
			Open R script in results folder and execute in Rstudio step by step. 
			
### How use data in WEKA? ###
	Execute the following query and select all the columns from table TrendsClassification. To select with headings, 
	you can click on the top left cell of the result grid, right click and select 	Copy with Headers.

	Paste the data in the Excel and save the file in the .csv format.

	To install the WEKA, following steps are required:
		1) install the WEKA by double clicking weka-3-6-10-x64.exe
		2) Go to the folder where WEKA is installed. In our case C:\Program Files\Weka-3-6 and click Weka.jar file
		3) This will open Weka, click on explorer button. It will open another screen where we can select data and perform different data mining algorithms.
		4) Click on Open file... button and select the data or names file (Files of type dropdown might need to change to .csv type). We have copied the data file in the Weka installation folder.
		5) Click on Save... button. From Files of type dropdown, select C4.5 file format (*.names), change the filename to trends and click Save.
		6) This will create two files with the same name but different extensions i.e. trends.data and trends.names. These files will be used in C5 and other classifiers.